
const express = require (`express`);
const dotenv = require(`dotenv`); 
const { default: mongoose } = require("mongoose");
dotenv.config();

const app = express();
const PORT = 3007;

app.use(express.json())
app.use(express.urlencoded({extended:true})) 

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection
db.on("error", console.error.bind(console, `connection error:`))
db. once("open", () => console.log(`Connected to Database`))

const userSchema = new mongoose.Schema({
   userName:{
       type: String,
       required: [true, `Username is required`]
   },
})

const User = mongoose.model(`User`, userSchema)

app.post(`/signup`, (req,res) => {

   let newUser = new User({
       userName: req.body.username,
       password: req.body.password
   })

   newUser.save().then((result,err) => {
       if(result){
           return res.status(200).send(`User ${req.body.userName} is already registered`)
       } else {
           return res.status(500).json(err)
       }
   })
})

app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))

